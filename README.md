## Data Structure Considerations

When developing the Student Management System, several factors influenced the choice of data structure:

```plaintext
- Efficiency: The selected structure, an array of structures (`struct Student`), enables fast retrieval and manipulation of student records, crucial for system operations.

- Memory Efficiency: Arrays utilize memory efficiently by storing elements in contiguous memory locations, reducing overhead.

- Ease of Implementation: Arrays are straightforward to implement and maintain, simplifying development and debugging processes.

- Flexibility: The `struct Student` accommodates various student information types and allows for easy expansion if additional fields are needed in the future.
```

## Contributers
      GROUP 16 MEMBERS 
ATALA DEBORAH	23/U/06754/PS
BAINOMUGISHA DERRICK	23/U/0350
MUNEZERO NAOMI	23/U/23681
KABAJUNGU LINDSAY LILLIAN	23/U/2564/EXT
KASAKYA DENIS NICHOLAS	23/U/09112/PS

## Videos
BAINOMUGISHA DERRICK - https://youtu.be/HFs1Vfanrj8?si=smKKTYHykZT2tAZV
MUNEZERO NAOMI - https://youtu.be/GSv7sFEa4wI?si=4TubawnwhKKHOTGM
KASAKYA DENIS NICHOLAS - https://youtu.be/tqjdtp-Up9c?si=UlGlsrQ-afeUSzSY
KABAJUNGU LINDSAY LILLIAN - -https://youtu.be/A4h3A56pO3w
ATALA DEBORAH - https://youtu.be/26tNs8WqajQ