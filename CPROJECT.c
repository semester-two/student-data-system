#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100 // Maximum number of students that can be stored

// Define a structure to hold student data
struct Student {
    char name[50]; // Student's name
    char date_of_birth[10]; // Student's date of birth in YYYY-MM-DD format
    char registration_number[6]; // Student's registration number
    char program_code[4]; // Student's program code
    float tuition; // Student's annual tuition
};

struct Student students[MAX_STUDENTS]; // Array to store student data
int numStudents = 0; // Counter for the number of students

// Function to clear the input buffer
void clearInputBuffer() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

// Function prototypes for student management operations
void displayMenu(); // Display the menu options
void createStudent(); // Create a new student record
void displayStudents(); // Display all student records
void updateStudent(); // Update an existing student record
void deleteStudent(); // Delete a student record
void searchByRegistrationNumber(); // Search for a student by registration number
void sortStudents(); // Sort student records
void exportStudentsToCSV(); // Export student records to a CSV file

int main() {
    int choice;

    // Main loop to display menu and handle user choices
    do {
        displayMenu();
        printf("Enter your choice: ");
        scanf("%d", &choice);

        // Switch statement to handle user choice
        switch (choice) {
            case 1:
                createStudent();
                break;
            case 2:
                displayStudents();
                break;
            case 3:
                updateStudent();
                break;
            case 4:
                deleteStudent();
                break;
            case 5:
                searchByRegistrationNumber();
                break;
            case 6:
                sortStudents();
                break;
            case 7:
                exportStudentsToCSV();
                break;
            case 8:
                printf("exiting program...\n");
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 7);

    return 0;
}

// Function to display the menu options
void displayMenu() {
    printf("\n==============================\n");
    printf("   Student Management System\n");
    printf("==============================\n");
    printf("          Menu\n");
    printf("-------------------------------\n");
    printf("1. Create Student\n");
    printf("2. Display Students\n");
    printf("3. Update Student\n");
    printf("4. Delete Student\n");
    printf("5. Search by Registration Number\n");
    printf("6. Sort Students\n");
    printf("7. Export Students To CSV\n");
    printf("8. Exit\n");
    printf("-------------------------------\n");
}

// Function to create a new student record
void createStudent() {
    if (numStudents >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }

    struct Student newStudent;

    printf("Enter student's name: ");
    scanf(" %50[^\n]", newStudent.name);
    clearInputBuffer();

    printf("Enter student's date of birth (YYYY-MM-DD): ");
    scanf(" %10s", newStudent.date_of_birth);
    clearInputBuffer();

    printf("Enter student's registration number: ");
    scanf(" %6s", newStudent.registration_number);
    clearInputBuffer();

    printf("Enter student's program code: ");
    scanf(" %4s", newStudent.program_code);
    clearInputBuffer();

    printf("Enter student's annual tuition: ");
    scanf(" %f", &newStudent.tuition);
    if (newStudent.tuition==0){
        printf(" Error:Tuition should not be zero.");
        return 1;
    }

    students[numStudents++] = newStudent;

    printf("Student created successfully.\n");
}

// Function to display all student records
void displayStudents() {
    if (numStudents == 0) {
        printf("No students found.\n");
        return;
    }

    printf("List of students:\n");
    for (int i = 0; i < numStudents; ++i) {
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].registration_number);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", students[i].tuition);
        printf("---------------------------\n");
    }
}

// Function to update an existing student record
void updateStudent() {
    if (numStudents == 0) {
        printf("No students to update.\n");
        return;
    }

    char regNumber[7];
    printf("Enter the registration number of the student to update: ");
    scanf("%s", regNumber);

    int foundIndex = -1;
    for (int i = 0; i < numStudents; i++) {
        if (strcmp(students[i].registration_number, regNumber) == 0) {
            foundIndex = i;
            break;
        }
    }

    if (foundIndex != -1) {
        printf("Student found. Enter new information:\n");
        printf("Enter student's name: ");
        scanf(" %[^\n]", students[foundIndex].name);

        printf("Enter student's date of birth (YYYY-MM-DD): ");
        scanf("%s", students[foundIndex].date_of_birth);

        printf("Enter student's program code (max 4 characters): ");
        scanf("%s", students[foundIndex].program_code);

        printf("Enter student's tuition: ");
        scanf("%f", &students[foundIndex].tuition);

        printf("Student information updated successfully.\n");
    } else {
        printf("Student with registration number %s not found.\n", regNumber);
    }
}

// Function to delete a student record
void deleteStudent() {
    char regNumber[7];
    printf("Enter the registration number of the student to delete: ");
    scanf("%s", regNumber);

    int foundIndex = -1;
    for (int i = 0; i < numStudents; i++) {
        if (strcmp(students[i].registration_number, regNumber) == 0) {
            foundIndex = i;
            break;
        }
    }

    if (foundIndex != -1) {
        for (int i = foundIndex; i < numStudents - 1; i++) {
            students[i] = students[i + 1];
        }
        numStudents--;
        printf("Student deleted successfully.\n");
    } else {
        printf("Student with registration number %s not found.\n", regNumber);
    }
}

// Function to search for a student by registration number
void searchByRegistrationNumber() {
    char regNumber[7];
    printf("Enter the registration number of the student to search: ");
    scanf("%s", regNumber);

    int foundIndex = -1;
    for (int i = 0; i < numStudents; i++) {
        if (strcmp(students[i].registration_number, regNumber) == 0) {
            foundIndex = i;
            break;
        }
    }

    if (foundIndex != -1) {
        printf("Student found:\n");
        printf("Name: %s\n", students[foundIndex].name);
        printf("Date of Birth: %s\n", students[foundIndex].date_of_birth);
        printf("Registration Number: %s\n", students[foundIndex].registration_number);
        printf("Program Code: %s\n", students[foundIndex].program_code);
        printf("Tuition: %.2f\n", students[foundIndex].tuition);
    } else {
        printf("Student with registration number %s not found.\n", regNumber);
    }
}

// Function to sort student records
void sortStudents() {
    int sortChoice;
    printf("Sort by:\n");
    printf("1. Name\n");
    printf("2. Date of Birth\n");
    printf("3. Registration Number\n");
    printf("4. Program Code\n");
    printf("5. Tuition\n");
    printf("Enter your choice: ");
    scanf("%d", &sortChoice);

    switch (sortChoice) {
        case 1:
            // Sort by name
            for (int i = 0; i < numStudents - 1; i++) {
                for (int j = 0; j < numStudents - i - 1; j++) {
                    if (strcmp(students[j].name, students[j + 1].name) > 0) {
                        struct Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            break;
        case 2:
            // Sort by date of birth
            break;
        case 3:
            // Sort by registration number
            break;
        case 4:
            // Sort by program code
            break;
        case 5:
            // Sort by tuition
            break;
        default:
            printf("Invalid choice. Please try again.\n");
            return;
    }

    // Print the sorted list
    printf("Sorted list of students:\n");
    displayStudents();
}

void exportStudentsToCSV(){
    FILE *file = fopen("students.csv","a");
    if (file == NULL){
        printf("Error opening file for writing.\n");
        return;
    }
    if (ftell(file) ==0){
        fprintf(file, "Name, Date of Birth, Registration Number,Program Code, Tuition\n");
    }
    for (int i = 0; i < numStudents; ++i) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n",
                students[i].name,
                students[i].date_of_birth,
                students[i].registration_number,
                students[i].program_code,
                students[i].tuition);
    }
    fclose(file);
    printf("Students exported to CSV successfully.\n");
}
